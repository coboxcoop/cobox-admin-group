const { describe } = require('tape-plus')
const crypto = require('cobox-crypto')
const path = require('path')
const fs = require('fs')
const mkdirp = require('mkdirp')
const randomWords = require('random-words')
const sinon = require('sinon')
const proxyquire = require('proxyquire')
const Kappa = require('kappa-core')
const collect = require('collect-stream')
const { encodings } = require('cobox-schemas')
const Announce = encodings.command.announce
const Hide = encodings.command.hide
const { tmp, cleanup } = require('./util')
const Admin = require('../')

describe('cobox-admin: Admin', (context) => {
  context('constructor() - generates a new encryption key', async (assert, next) => {
    var storage = tmp(),
      address = crypto.address(),
      name = randomWords(1).pop()

    var spy = sinon.spy(crypto)
    var ProxyAdmin = proxyquire('../', { 'cobox-crypto': spy })
    var admin = ProxyAdmin(storage, address, { name })

    assert.ok(admin, 'admin created')
    assert.same(admin.address, Buffer.from(address, 'hex'), 'has address')
    assert.same(admin.name, name, 'has name')
    assert.ok(admin.core instanceof Kappa, 'has a kappa-core')
    assert.ok(spy.encryptionKey.calledOnce, 'calls crypto.encryptionKey()')
    assert.ok(spy.encoder.calledOnce, 'uses encryption_key in encoder')

    admin.ready(() => {
      assert.ok(admin.log, 'has a log')
      cleanup(storage, next)
    })
  })

  context('constructor() - loads encryption_key from path', (assert, next) => {
    var storage = tmp()
    var address = crypto.address()
    var encryptionKey = crypto.encryptionKey()
    var keyPath = path.join(storage, address.toString('hex'), 'encryption_key')

    mkdirp.sync(path.dirname(keyPath))
    fs.writeFileSync(keyPath, encryptionKey, { mode: fs.constants.S_IRUSR })

    var loadStub = sinon.stub().returns(encryptionKey)
    var saveStub = sinon.stub().returns(true)

    var ProxyAdmin = proxyquire('../', { 'cobox-keys': {
      loadKey: loadStub,
      saveKey: saveStub
    }})

    var admin = ProxyAdmin(storage, address)

    assert.ok(loadStub.calledWith(admin.path, 'encryption_key'), 'key is loaded')
    assert.ok(saveStub.calledWith(admin.path, 'encryption_key', encryptionKey), 'key is saved')
    cleanup(storage, next)
  })

  context('constructor() - saves encryption_key given as argument', (assert, next) => {
    var storage = tmp()
    var address = crypto.address()
    var encryptionKey = crypto.encryptionKey()
    var admin = Admin(storage, address, { encryptionKey })
    var key = fs.readFileSync(path.join(admin.path, 'encryption_key'))

    assert.ok(admin, 'admin loaded')
    assert.same(admin.address, address, 'same address')
    assert.same(key.toString('hex'), encryptionKey.toString('hex'), 'encryption_key stored correctly')

    cleanup(storage, next)
  })

  context('broadcast', async (assert, next) => {
    var storage = tmp(),
      address = crypto.address(),
      encryptionKey = crypto.encryptionKey(),
      count = 0

    var admin = Admin(storage, address, { encryptionKey })

    await admin.ready()

    await admin.log.publish(Announce.encode({
      type: 'command/announce',
      timestamp: Date.now(),
      author: admin.identity.publicKey.toString('hex')
    }))

    await admin.log.publish(Hide.encode({
      type: 'command/hide',
      timestamp: Date.now(),
      author: admin.identity.publicKey.toString('hex')
    }))

    await admin.log.ready()

    let data = await admin.log.query({
      $filter: {
        value: {
          type: { $in: ['command/announce', 'command/hide'] },
          timestamp: { $gt: 0 }
        }
      }
    })

    assert.same(data.length, 2, 'gets two messages')
    cleanup(storage, next)
  })
})
