const crypto = require('cobox-crypto')
const Admin = require('./')
const tmpdir = require('tmp').dirSync
const mkdirp = require('mkdirp')
const { encodings } = require('cobox-schemas')

const PeerAbout = encodings.peer.about

function tmp () {
  var path = '.'+tmpdir().name
  mkdirp.sync(path)
  return path
}

function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf.toString('hex')
  else return buf
}

async function Application () {
  const storage = tmp()
  const address = crypto.address()
  const encryptionKey = crypto.encryptionKey()
  const identity = crypto.boxKeyPair()
  const admin = Admin(storage, address, { encryptionKey, identity })

  await admin.ready()

  let query = [{ $filter: { value: { type: 'peer/about', timestamp: { gt: 0 } } } }]

  admin.log.read({ query }).on('data', console.log)

  await admin.log.publish(PeerAbout.encode({
    type: 'peer/about',
    author: hex(identity.publicKey),
    timestamp: Date.now(),
    content: { name: 'magpie' }
  }))

  admin.swarm()
}

Application()
