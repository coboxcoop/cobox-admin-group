const Kappa = require('kappa-core')
const through = require('through2')
const crypto = require('cobox-crypto')
const debug = require('debug')('cobox-hub')
const path = require('path')
const thunky = require('thunky')
const maybe = require('call-me-maybe')
const assert = require('assert')
const Log = require('cobox-log')
const { Replicator } = require('cobox-replicator')
const { setupLevel } = require('cobox-replicator/lib/level')
const { loadKey, saveKey } = require('cobox-keys')
const constants = require('cobox-constants')
const { keyIds } = constants
const { log: LOG_ID } = keyIds

const ENC_KEY = 'encryption_key'

class Admin extends Replicator {
  /**
   * Create an admin space
   * @constructor
   */
  constructor (storage, address, opts = {}) {
    super(storage, address, opts)

    var key = loadKey(this.path, ENC_KEY) || opts.encryptionKey
    var encryptionKey = crypto.encryptionKey(key)
    assert(crypto.isKey(encryptionKey), `invalid: ${ENC_KEY}`)
    saveKey(this.path, ENC_KEY, encryptionKey)

    this._deriveKeyPair = opts.deriveKeyPair || randomKeyPair
    this.core = new Kappa()

    this._initFeeds({
      valueEncoding: crypto.encoder(encryptionKey, Object.assign({
        valueEncoding: this._opts.valueEncoding || 'utf8'
      }, this._opts))
    })
  }

  ready (callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      super.ready((err) => {
        if (err) return reject(err)
        this.open((err) => {
          if (err) return reject(err)
          this.log.ready((err) => {
            if (err) return reject(err)
            this.core.ready((err) => {
              if (err) return reject(err)
              return resolve()
            })
          })
        })
      })
    }))
  }

  close (callback) {
    super.close((err) => {
      this.core.close(callback)
    })
  }

  createConnectionsStream () {
    var self = this
    return this.db.connections
      .createLiveStream()
      .pipe(through.obj(function (msg, _, next) {
        if (msg.sync) return next()
        this.push({
          resourceType: 'ADMIN_DEVICE',
          peerId: msg.key,
          address: hex(self.address),
          data: msg.value
        })
        next()
      }))
  }

  createLogStream () {
    var self = this
    let query = [{ $filter: { value: { timestamp: { $gt: 0 } } } }]
    return this.log
      .read({ query, live: true, old: true })
      .pipe(through.obj(function (msg, _, next) {
        if (msg.sync) return next()
        this.push({
          resourceType: 'ADMIN_DEVICE',
          feedId: msg.key,
          address: hex(self.address),
          data: msg.value
        })
        next()
      }))
  }

  // ------------------------------ PRIVATE FUNCTIONS ------------------------------

  _open (callback) {
    super._open((err) => {
      this.log = Log({
        core: this.core,
        feeds: this.feeds,
        author: hex(this.identity.publicKey),
        keyPair: this._deriveKeyPair(LOG_ID, this.address),
        db: setupLevel(path.join(this.path, 'views', 'log'))
      })

      return callback()
    })
  }
}

function randomKeyPair (id, context) {
  return crypto.keyPair(crypto.masterKey(), id, context)
}

function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf
  return buf.toString('hex')
}

module.exports = (storage, address, opts) => new Admin(storage, address, opts)
module.exports.Admin = Admin
