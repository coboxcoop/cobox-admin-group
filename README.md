# cobox-admin-group

**This package is not yet stable!**

An admin space is like a special kind of `cobox-group`. Rather than managing an archive of files together, it represents the space who manage a cobox hardware device, the admin space, allowing them to control which spaces it replicates.

This is done by sending encrypted messages to [cobox-hub](https://gitlab.com/coboxcoop/cobox-hub).

An admin space simply contains a `cobox-log` which uses a `kappa-core` and `multifeed` instance, inherited from `cobox-replicator`, `kappa-view-query` for querying the log, and wraps it all with a cryptographic value encoder so the log is fully encrypted.

# Install

```
npm install cobox-admin-group
```

# Usage

```js
const crypto = require('cobox-crypto')
const Admin = require('./')

const storage = tmp()
const address = crypto.address()
const encryptionKey = crypto.encryptionKey()
const admin = Admin(storage, address, { encryptionKey })

let query = { $filter: { value: { type: 'peer/about' } } }
let stream = admin.log.read({ query })
stream.on('data', console.log)

admin.log.publish({ type: 'peer/about', content: { name: 'Magpie' } })
admin.swarm()
```

## Contributions
